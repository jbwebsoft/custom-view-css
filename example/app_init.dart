library jb_menu.app_init;

import 'package:angular2/platform/browser.dart';

import 'package:logging/logging.dart';
import 'package:logging_handlers/logging_handlers_shared.dart';

//import own components
import 'app_component/app_component.dart';

final Logger _libLogger = new Logger("custom_view_css");

void main() {
  //init logging
  hierarchicalLoggingEnabled = true;
  Logger.root.onRecord.listen(new LogPrintHandler());

  Logger.root.level = Level.OFF;
  _libLogger.level = Level.ALL;

  bootstrap(AppComponent);
}

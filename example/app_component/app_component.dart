library template.app_component;

import 'package:angular2/angular2.dart';

import 'package:custom_view_css/custom_view_css.dart';
import 'package:custom_view_css/test.dart';

@Component(selector: 'app-root',
    templateUrl: "app_component.html",
    directives: const [CustomViewCss, TestComponent, Test2Component])
class AppComponent {


}
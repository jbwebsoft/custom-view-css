library custom_view_css.test_component;

import 'package:angular2/angular2.dart';

@Component(
    selector: "test-component",
    templateUrl: "test_component.html",
    directives: const []
)
class TestComponent {
  String greetingPerson = "Bob";
}
library custom_view_css.directive;

import 'package:angular2/angular2.dart';
import 'package:angular2/platform/browser.dart';
import 'package:logging/logging.dart';
import 'dart:html';

@Directive(selector: "[customViewCss]")
class CustomViewCss implements OnInit {
  Logger _log = new Logger("custom_view_css.component");
  ViewContainerRef viewContainer;
  ElementRef element;
  Renderer renderer;

  ///list of custom css files
  @Input()
  List<String> customViewCss;

  CustomViewCss(this.viewContainer, this.element, this.renderer) {}

  @override
  ngOnInit() {
    if (customViewCss != null) {
      BrowserDomAdapter domAdapter = new BrowserDomAdapter();
      dynamic parent;

      if (domAdapter.supportsNativeShadowDOM()) {
        parent = domAdapter.getShadowRoot(element.nativeElement);
      }

      if (parent == null) {
        parent = element.nativeElement;
      }

      //creates dynamic <style> @import "../my/path/style.css"; </style> instead of
      // <link rel="stylesheet" type="text/css" [href]="safeCustomCssUrl">
      // because <link> is not supported in shadow root
      customViewCss.forEach((String customCss) {
        StyleElement styleElement = renderer.createElement(parent, "style", null);
        styleElement
          ..type = "text/css"
          ..text = '''
        @import "$customCss";
      ''';
      });

      //TODO: Scoping of styles gets lost in emulated ViewEncapsulation mode
      //TODO: Styles are not transcludet into shadow-root
    }
  }
}

# OUTDATED - superseeded by sass solution / css custom properties

# custom-view-css


## Usage

A simple usage example:

    import 'package:template/template.dart';

    main() {
      var awesome = new Awesome();
    }

## Features and bugs

Please file feature requests and bugs at the [issue tracker][tracker].

[tracker]: http://example.com/issues/replaceme
